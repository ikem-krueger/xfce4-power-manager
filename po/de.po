# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the xfce4-power-manager.master package.
# 
# Translators:
# Andreas Eitel <github-aneitel@online.de>, 2019-2021,2023
# Andreas Eitel <github-aneitel@online.de>, 2019
# Christian Weiske, 2010
# Christoph Wickert, 2010
# Ettore Atalan <atalanttore@googlemail.com>, 2017
# Fabian Nowak <timystery@arcor.de>, 2008,2010
# Harald Judt <h.judt@gmx.at>, 2013-2014
# Hubert Hesse, 2010
# Johannes Lips <johannes.lips@gmail.com>, 2013-2014
# Mark Trompell <mark@marktrompell.de>, 2008
# Paul Seyfert, 2010
# Tobias Bannert <tobannert@gmail.com>, 2014-2018
# Vinzenz Vietzke <vinz@vinzv.de>, 2017-2018
msgid ""
msgstr ""
"Project-Id-Version: Xfce4-power-manager\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2023-11-26 00:47+0100\n"
"PO-Revision-Date: 2013-07-02 20:42+0000\n"
"Last-Translator: Andreas Eitel <github-aneitel@online.de>, 2019-2021,2023\n"
"Language-Team: German (http://app.transifex.com/xfce/xfce4-power-manager/language/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/interfaces/xfpm-settings.ui:98 settings/xfpm-settings.c:626
#: settings/xfpm-settings.c:641 settings/xfpm-settings.c:670
#: settings/xfpm-settings.c:1764
msgid "Never"
msgstr "Nie"

#: data/interfaces/xfpm-settings.ui:102
msgid "When the screensaver is activated"
msgstr "Wenn der Bildschirmschoner aktiviert ist"

#: data/interfaces/xfpm-settings.ui:106
msgid "When the screensaver is deactivated"
msgstr "Wenn der Bildschirmschoner deaktiviert ist"

#: data/interfaces/xfpm-settings.ui:117 data/interfaces/xfpm-settings.ui:128
#: data/interfaces/xfpm-settings.ui:139 data/interfaces/xfpm-settings.ui:150
#: data/interfaces/xfpm-settings.ui:161 data/interfaces/xfpm-settings.ui:172
#: data/interfaces/xfpm-settings.ui:183 data/interfaces/xfpm-settings.ui:194
#: data/interfaces/xfpm-settings.ui:205 data/interfaces/xfpm-settings.ui:216
msgid "Nothing"
msgstr "Nichts unternehmen"

#: data/interfaces/xfpm-settings.ui:238
#: settings/xfce4-power-manager-settings.desktop.in:4
#: settings/xfce4-power-manager-settings.desktop.in:5 src/xfpm-power.c:237
#: src/xfpm-power.c:502 src/xfpm-power.c:531 src/xfpm-power.c:705
#: src/xfpm-power.c:726 src/xfpm-backlight.c:166 src/xfpm-backlight.c:174
#: src/xfpm-battery.c:190 src/xfpm-kbd-backlight.c:119
#: src/xfce4-power-manager.desktop.in:3
msgid "Power Manager"
msgstr "Energieverwaltung"

#: data/interfaces/xfpm-settings.ui:253
#: panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:157
msgid "_Help"
msgstr "_Hilfe"

#: data/interfaces/xfpm-settings.ui:268
#: panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:158
msgid "_Close"
msgstr "S_chließen"

#: data/interfaces/xfpm-settings.ui:330
msgid "When power button is pressed:"
msgstr "Bei Betätigung der Ein-/Austaste:"

#: data/interfaces/xfpm-settings.ui:342
msgid "When sleep button is pressed:"
msgstr "Wenn die Energiesparmodustaste betätigt wird:"

#: data/interfaces/xfpm-settings.ui:354
msgid "When hibernate button is pressed:"
msgstr "Bei Betätigung der Taste für den Ruhezustand:"

#: data/interfaces/xfpm-settings.ui:366
msgid "When battery button is pressed:"
msgstr "Bei Betätigung der Akkutaste:"

#: data/interfaces/xfpm-settings.ui:481
msgid "Exponential"
msgstr "Exponentiell"

#: data/interfaces/xfpm-settings.ui:504
msgid "Brightness step count:"
msgstr "Helligkeitsschrittweite:"

#: data/interfaces/xfpm-settings.ui:515
msgid "Handle display brightness _keys"
msgstr "_Bildschirmhelligkeitstasten steuern"

#: data/interfaces/xfpm-settings.ui:538
msgid "<b>Buttons</b>"
msgstr "<b>Knöpfe</b>"

#: data/interfaces/xfpm-settings.ui:572
msgid "Status notifications"
msgstr "Statusbenachrichtigungen"

#: data/interfaces/xfpm-settings.ui:610
msgid "System tray icon"
msgstr "Benachrichtigungsfeldsymbol"

#: data/interfaces/xfpm-settings.ui:646
msgid "<b>Appearance</b>"
msgstr "<b>Aussehen</b>"

#: data/interfaces/xfpm-settings.ui:666
msgid "General"
msgstr "Allgemein"

#: data/interfaces/xfpm-settings.ui:711 data/interfaces/xfpm-settings.ui:949
msgid "When inactive for"
msgstr "Bei Inaktivität von"

#: data/interfaces/xfpm-settings.ui:785 data/interfaces/xfpm-settings.ui:1023
msgid "System sleep mode:"
msgstr "Energiesparmodus:"

#: data/interfaces/xfpm-settings.ui:797 data/interfaces/xfpm-settings.ui:1035
msgid "<b>System power saving</b>"
msgstr "<b>Energiesparen des Systems</b>"

#: data/interfaces/xfpm-settings.ui:812 data/interfaces/xfpm-settings.ui:934
msgid "<b>Laptop Lid</b>"
msgstr "<b>Laptopbildschirm</b>"

#: data/interfaces/xfpm-settings.ui:827 data/interfaces/xfpm-settings.ui:903
msgid "When laptop lid is closed:"
msgstr "Beim Schließen des Laptopbildschirms:"

#: data/interfaces/xfpm-settings.ui:857 data/interfaces/xfpm-settings.ui:1049
msgid "<b>Power profile</b>"
msgstr "<b>Energieprofil</b>"

#: data/interfaces/xfpm-settings.ui:889 data/interfaces/xfpm-settings.ui:1608
msgid "On battery"
msgstr "Akku wird entladen"

#: data/interfaces/xfpm-settings.ui:1081 data/interfaces/xfpm-settings.ui:1865
#: common/xfpm-power-common.c:416
msgid "Plugged in"
msgstr "Strom angeschlossen"

#: data/interfaces/xfpm-settings.ui:1117
msgid "Critical battery power level:"
msgstr "Kritischer Akkuladezustand:"

#: data/interfaces/xfpm-settings.ui:1148 settings/xfpm-settings.c:689
msgid "%"
msgstr "%"

#: data/interfaces/xfpm-settings.ui:1172
msgid "On critical battery power:"
msgstr "Bei kritischer Akkuladung:"

#: data/interfaces/xfpm-settings.ui:1212
msgid "<b>Critical power</b>"
msgstr "<b>Kritische Energie</b>"

#: data/interfaces/xfpm-settings.ui:1241 data/interfaces/xfpm-settings.ui:2008
msgid "Lock screen when system is going to sleep"
msgstr "Bildschirm sperren, wenn das System in den Energiesparmodus geht."

#: data/interfaces/xfpm-settings.ui:1261
msgid "<b>Security</b>"
msgstr "<b>Sicherheit</b>"

#: data/interfaces/xfpm-settings.ui:1281
msgid "System"
msgstr "System"

#: data/interfaces/xfpm-settings.ui:1320
msgid "<b>Display power management</b>"
msgstr "<b>Bildschirmenergieverwaltung</b>"

#: data/interfaces/xfpm-settings.ui:1333
msgid ""
"Let the power manager handle display power management (DPMS) instead of X11."
msgstr "Die Energieverwaltung die Bildschirmenergieverwaltung (DPMS) verarbeiten lassen, anstelle von X11."

#: data/interfaces/xfpm-settings.ui:1410 data/interfaces/xfpm-settings.ui:1622
msgid "Put to sleep after"
msgstr "Energiesparmodus nach"

#: data/interfaces/xfpm-settings.ui:1423 data/interfaces/xfpm-settings.ui:1635
msgid "Switch off after"
msgstr "Ausschalten nach"

#: data/interfaces/xfpm-settings.ui:1481 data/interfaces/xfpm-settings.ui:1664
msgid "On inactivity reduce to"
msgstr "Bei Inaktivität reduzieren auf"

#: data/interfaces/xfpm-settings.ui:1494 data/interfaces/xfpm-settings.ui:1677
msgid "Reduce after"
msgstr "Reduzieren nach"

#: data/interfaces/xfpm-settings.ui:1595 data/interfaces/xfpm-settings.ui:1648
msgid "<b>Brightness reduction</b>"
msgstr "<b>Helligkeitsreduzierung</b>"

#: data/interfaces/xfpm-settings.ui:1885
msgid "Display"
msgstr "Bildschirm"

#: data/interfaces/xfpm-settings.ui:1924
msgid "Automatically lock the session:"
msgstr "Die Sitzung automatisch sperren:"

#: data/interfaces/xfpm-settings.ui:1955
msgid "Delay locking after screensaver for"
msgstr "Sperrverzögerung nach Bildschirmschonerstart"

#: data/interfaces/xfpm-settings.ui:2036
msgid "<b>Light Locker</b>"
msgstr "<b>Light-Locker</b>"

#: data/interfaces/xfpm-settings.ui:2056
msgid "Security"
msgstr "Sicherheit"

#: settings/xfpm-settings.c:629 settings/xfpm-settings.c:679
#: settings/xfpm-settings.c:680
msgid "One minute"
msgstr "Eine Minute"

#: settings/xfpm-settings.c:631 settings/xfpm-settings.c:643
#: settings/xfpm-settings.c:654 settings/xfpm-settings.c:658
#: settings/xfpm-settings.c:682 settings/xfpm-settings.c:683
#: settings/xfpm-settings.c:1773
msgid "minutes"
msgstr "Minuten"

#: settings/xfpm-settings.c:645 settings/xfpm-settings.c:652
#: settings/xfpm-settings.c:653 settings/xfpm-settings.c:654
msgid "One hour"
msgstr "Eine Stunde"

#: settings/xfpm-settings.c:653 settings/xfpm-settings.c:657
msgid "one minute"
msgstr "eine Minute"

#: settings/xfpm-settings.c:656 settings/xfpm-settings.c:657
#: settings/xfpm-settings.c:658
msgid "hours"
msgstr "Stunden"

#: settings/xfpm-settings.c:677 settings/xfpm-settings.c:680
#: settings/xfpm-settings.c:683 settings/xfpm-settings.c:1766
msgid "seconds"
msgstr "Sekunden"

#: settings/xfpm-settings.c:907 settings/xfpm-settings.c:986
#: settings/xfpm-settings.c:1048 settings/xfpm-settings.c:1196
#: settings/xfpm-settings.c:1288 settings/xfpm-settings.c:1455
#: settings/xfpm-settings.c:1513 settings/xfpm-settings.c:1565
#: settings/xfpm-settings.c:1616 src/xfpm-power.c:558
msgid "Suspend"
msgstr "Bereitschaft"

#: settings/xfpm-settings.c:911 settings/xfpm-settings.c:1200
msgid "Suspend operation not permitted"
msgstr "Versetzen in den Bereitschaftsmodus ist nicht erlaubt"

#: settings/xfpm-settings.c:915 settings/xfpm-settings.c:1204
msgid "Suspend operation not supported"
msgstr "Versetzen in den Bereitschaftsmodus wird nicht unterstützt"

#: settings/xfpm-settings.c:921 settings/xfpm-settings.c:992
#: settings/xfpm-settings.c:1054 settings/xfpm-settings.c:1210
#: settings/xfpm-settings.c:1294 settings/xfpm-settings.c:1461
#: settings/xfpm-settings.c:1519 settings/xfpm-settings.c:1571
#: settings/xfpm-settings.c:1622 src/xfpm-power.c:546
msgid "Hibernate"
msgstr "Ruhezustand"

#: settings/xfpm-settings.c:925 settings/xfpm-settings.c:1214
msgid "Hibernate operation not permitted"
msgstr "Versetzen in den Ruhezustand ist nicht erlaubt"

#: settings/xfpm-settings.c:929 settings/xfpm-settings.c:1218
msgid "Hibernate operation not supported"
msgstr "Ruhezustand wird nicht unterstützt"

#: settings/xfpm-settings.c:959 settings/xfpm-settings.c:1248
#: settings/xfpm-settings.c:1722 settings/xfpm-settings.c:1849
msgid "Hibernate and suspend operations not supported"
msgstr "Ruhezustand- und Bereitschaftsvorgänge werden nicht unterstützt"

#: settings/xfpm-settings.c:964 settings/xfpm-settings.c:1253
#: settings/xfpm-settings.c:1727 settings/xfpm-settings.c:1854
msgid "Hibernate and suspend operations not permitted"
msgstr "Ruhezustand und Bereitschaftsvorgänge sind nicht erlaubt"

#: settings/xfpm-settings.c:981 settings/xfpm-settings.c:1061
#: settings/xfpm-settings.c:1301 settings/xfpm-settings.c:1450
#: settings/xfpm-settings.c:1508 settings/xfpm-settings.c:1560
#: settings/xfpm-settings.c:1611
msgid "Do nothing"
msgstr "Nichts unternehmen"

#: settings/xfpm-settings.c:998 settings/xfpm-settings.c:1467
#: src/xfpm-power.c:578
msgid "Shutdown"
msgstr "Herunterfahren"

#: settings/xfpm-settings.c:1002 settings/xfpm-settings.c:1471
#: settings/xfpm-settings.c:1523 settings/xfpm-settings.c:1575
#: settings/xfpm-settings.c:1626
msgid "Ask"
msgstr "Nachfragen"

#: settings/xfpm-settings.c:1043 settings/xfpm-settings.c:1283
msgid "Switch off display"
msgstr "Bildschirm ausschalten"

#: settings/xfpm-settings.c:1058 settings/xfpm-settings.c:1298
msgid "Lock screen"
msgstr "Bildschirm sperren"

#: settings/xfpm-settings.c:1655
msgid "Number of brightness steps available using keys"
msgstr "Anzahl der über Tasten verfügbaren Helligkeitsstufen"

#: settings/xfpm-settings.c:1695
msgid "When all the power sources of the computer reach this charge level"
msgstr "Wenn alle Stromquellen des Rechners diese Stufe erreichen"

#: settings/xfpm-settings.c:1771 common/xfpm-power-common.c:166
msgid "minute"
msgid_plural "minutes"
msgstr[0] "Minute"
msgstr[1] "Minuten"

#: settings/xfpm-settings.c:2137
msgid "Device"
msgstr "Gerät"

#: settings/xfpm-settings.c:2160
msgid "Type"
msgstr "Typ"

#: settings/xfpm-settings.c:2165
msgid "PowerSupply"
msgstr "Energieversorgung"

#: settings/xfpm-settings.c:2166 src/xfpm-main.c:79
msgid "True"
msgstr "Ja"

#: settings/xfpm-settings.c:2166 src/xfpm-main.c:79
msgid "False"
msgstr "Nein"

#: settings/xfpm-settings.c:2173
msgid "Model"
msgstr "Modell"

#: settings/xfpm-settings.c:2176
msgid "Technology"
msgstr "Technologie"

#: settings/xfpm-settings.c:2183
msgid "Current charge"
msgstr "Aktuelle Ladung"

#. TRANSLATORS: Unit here is Watt hour
#: settings/xfpm-settings.c:2191 settings/xfpm-settings.c:2203
#: settings/xfpm-settings.c:2215
msgid "Wh"
msgstr "Wh"

#: settings/xfpm-settings.c:2193
msgid "Fully charged (design)"
msgstr "Vollständig geladen (Entwurf)"

#: settings/xfpm-settings.c:2206
msgid "Fully charged"
msgstr "Vollständig geladen"

#: settings/xfpm-settings.c:2217
msgid "Energy empty"
msgstr "Keine Energie"

#. TRANSLATORS: Unit here is Watt
#: settings/xfpm-settings.c:2225
msgid "W"
msgstr "W"

#: settings/xfpm-settings.c:2227
msgid "Energy rate"
msgstr "Energierate"

#. TRANSLATORS: Unit here is Volt
#: settings/xfpm-settings.c:2235
msgid "V"
msgstr "V"

#: settings/xfpm-settings.c:2237
msgid "Voltage"
msgstr "Spannung"

#: settings/xfpm-settings.c:2244
msgid "Vendor"
msgstr "Hersteller"

#: settings/xfpm-settings.c:2249
msgid "Serial"
msgstr "Seriell"

#: settings/xfpm-settings.c:2531
msgid "Check your power manager installation"
msgstr "Die Installation der Energieverwaltung überprüfen"

#: settings/xfpm-settings.c:2612
msgid "Devices"
msgstr "Geräte"

#: settings/xfpm-settings-app.c:76
msgid "Settings manager socket"
msgstr "Socket der Sitzungsverwaltung"

#: settings/xfpm-settings-app.c:76
msgid "SOCKET ID"
msgstr "SOCKET-KENNUNG"

#: settings/xfpm-settings-app.c:77
msgid "Display a specific device by UpDevice object path"
msgstr "Ein bestimmtes Gerät vom UpDevice-Objektpfad anzeigen"

#: settings/xfpm-settings-app.c:77
msgid "UpDevice object path"
msgstr "UpDevice-Objektpfad"

#: settings/xfpm-settings-app.c:78 src/xfpm-main.c:273
msgid "Enable debugging"
msgstr "Fehlerdiagnose aktivieren"

#: settings/xfpm-settings-app.c:79
msgid "Display version information"
msgstr "Bildschirmversionsinformation"

#: settings/xfpm-settings-app.c:80
msgid "Cause xfce4-power-manager-settings to quit"
msgstr "Xfce-Energieeinstellungseinstellungen beenden"

#: settings/xfpm-settings-app.c:169 settings/xfpm-settings-app.c:216
#: src/xfpm-main.c:448
msgid "Xfce Power Manager"
msgstr "Xfce-Energieverwaltung"

#: settings/xfpm-settings-app.c:171
msgid "Failed to connect to power manager"
msgstr "Verbindung zur Energieverwaltung fehlgeschlagen"

#: settings/xfpm-settings-app.c:185
msgid "Xfce4 Power Manager is not running, do you want to launch it now?"
msgstr "Die Xfce-Energieverwaltung läuft nicht. Möchten Sie sie jetzt starten?"

#: settings/xfpm-settings-app.c:218
msgid "Failed to load power manager configuration, using defaults"
msgstr "Die Konfiguration der Energieverwaltung konnte nicht geladen werden; Vorgaben werden benutzt"

#: settings/xfpm-settings-app.c:344
#, c-format
msgid "This is %s version %s, running on Xfce %s.\n"
msgstr "Das ist %s Version %s und läuft auf Xfce %s.\n"

#: settings/xfpm-settings-app.c:346
#, c-format
msgid "Built with GTK+ %d.%d.%d, linked with GTK+ %d.%d.%d."
msgstr "Erstellt mit GTK+ %d.%d.%d, verknüpft mit GTK+ %d.%d.%d."

#: settings/xfce4-power-manager-settings.desktop.in:6
msgid "Settings for the Xfce Power Manager"
msgstr "Einstellungen für die Xfce-Energieverwaltung"

#: settings/xfce4-power-manager-settings.desktop.in:12
msgid ""
"settings;preferences;buttons;sleep;hibernate;battery;suspend;shutdown;brightness;laptop"
" lid;lock screen;plugged in;saving;critical;"
msgstr "Einstellungen;Präferenzen;Knöpfe;Energiesparmodus;Ruhezustand;Akku;Bereitschaft;Herunterfahren;Helligkeit;Laptop-Deckel;Bildschirm sperren;eingesteckt;Speichern;kritisch;"

#: common/xfpm-common.c:121
msgid "translator-credits"
msgstr "Fabian Nowak\nMark Trompell\nChristoph Wickert\nChristian Weiske\nHubert Hesse\nPaul Seyfert\nJohannes Lips\nHarald Judt\nTobias Bannert"

#: common/xfpm-power-common.c:45 common/xfpm-power-common.c:68
msgid "Battery"
msgstr "Akku"

#: common/xfpm-power-common.c:47
msgid "Uninterruptible Power Supply"
msgstr "Unterbrechungsfreie Stromversorgung"

#: common/xfpm-power-common.c:49
msgid "Line power"
msgstr "Netzstrom"

#: common/xfpm-power-common.c:51
msgid "Mouse"
msgstr "Maus"

#: common/xfpm-power-common.c:53
msgid "Keyboard"
msgstr "Tastatur"

#: common/xfpm-power-common.c:55
msgid "Monitor"
msgstr "Bildschirm"

#: common/xfpm-power-common.c:57
msgid "PDA"
msgstr "PDA"

#: common/xfpm-power-common.c:59
msgid "Phone"
msgstr "Telefon"

#: common/xfpm-power-common.c:61
msgid "Tablet"
msgstr "Tablet"

#: common/xfpm-power-common.c:63 common/xfpm-power-common.c:299
msgid "Computer"
msgstr "Rechner"

#: common/xfpm-power-common.c:65 common/xfpm-power-common.c:81
#: common/xfpm-power-common.c:96
msgid "Unknown"
msgstr "Unbekannt"

#: common/xfpm-power-common.c:83
msgid "Lithium ion"
msgstr "Lithium-Ionen"

#: common/xfpm-power-common.c:85
msgid "Lithium polymer"
msgstr "Lithium-Polymere"

#: common/xfpm-power-common.c:87
msgid "Lithium iron phosphate"
msgstr "Lithium-Eisen-Phosphat"

#: common/xfpm-power-common.c:89
msgid "Lead acid"
msgstr "Bleisäure"

#: common/xfpm-power-common.c:91
msgid "Nickel cadmium"
msgstr "Nickel-Cadmium"

#: common/xfpm-power-common.c:93
msgid "Nickel metal hydride"
msgstr "Nickel-Metall-Hydrid"

#: common/xfpm-power-common.c:141
msgid "Unknown time"
msgstr "Unbekannte Zeit"

#: common/xfpm-power-common.c:147
#, c-format
msgid "%i minute"
msgid_plural "%i minutes"
msgstr[0] "%i Minute"
msgstr[1] "%i Minuten"

#: common/xfpm-power-common.c:158
#, c-format
msgid "%i hour"
msgid_plural "%i hours"
msgstr[0] "%i Stunde"
msgstr[1] "%i Stunden"

#. TRANSLATOR: "%i %s %i %s" are "%i hours %i minutes"
#. * Swap order with "%2$s %2$i %1$s %1$i if needed
#: common/xfpm-power-common.c:164
#, c-format
msgid "%i %s %i %s"
msgstr "%i %s und %i %s"

#: common/xfpm-power-common.c:165
msgid "hour"
msgid_plural "hours"
msgstr[0] "Stunde"
msgstr[1] "Stunden"

#: common/xfpm-power-common.c:339
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Fully charged - %s remaining"
msgstr "<b>%s %s</b>\nVollständig aufgeladen - %s verbleibend"

#: common/xfpm-power-common.c:346
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Fully charged"
msgstr "<b>%s %s</b>\nVollständig geladen"

#: common/xfpm-power-common.c:355
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%0.0f%% - %s until full"
msgstr "<b>%s %s</b>\n%0.0f%% - %s bis vollständig geladen"

#: common/xfpm-power-common.c:363 common/xfpm-power-common.c:381
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%0.0f%%"
msgstr "<b>%s %s</b>\n%0.0f%%"

#: common/xfpm-power-common.c:373
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%0.0f%% - %s remaining"
msgstr "<b>%s %s</b>\n%0.0f%% - %s verbleibend"

#: common/xfpm-power-common.c:388
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Waiting to charge (%0.0f%%)"
msgstr "<b>%s %s</b>\nAuf das Laden wird gewartet (%0.0f%%)"

#: common/xfpm-power-common.c:394
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Waiting to discharge (%0.0f%%)"
msgstr "<b>%s %s</b>\nAuf das Entladen wird gewartet (%0.0f%%)"

#: common/xfpm-power-common.c:400
#, c-format
msgid ""
"<b>%s %s</b>\n"
"is empty"
msgstr "<b>%s %s</b>\nist leer"

#: common/xfpm-power-common.c:405
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Current charge: %0.0f%%"
msgstr "<b>%s%s</b>\nAktuelle Ladung: %0.0f%%"

#. On the 2nd line we want to know if the power cord is plugged
#. * in or not
#: common/xfpm-power-common.c:415
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%s"
msgstr "<b>%s %s</b>\n%s"

#: common/xfpm-power-common.c:416
msgid "Not plugged in"
msgstr "Nicht angeschlossen"

#. Desktop pc with no battery, just display the vendor and model,
#. * which will probably just be Computer
#: common/xfpm-power-common.c:422
#, c-format
msgid "<b>%s %s</b>"
msgstr "<b>%s %s</b>"

#. unknown device state, just display the percentage
#: common/xfpm-power-common.c:427
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Unknown state"
msgstr "<b>%s %s</b>\nUnbekannter Zustand"

#: src/xfpm-power.c:260
msgid ""
"An application is currently disabling the automatic sleep. Doing this action now may damage the working state of this application.\n"
"Are you sure you want to hibernate the system?"
msgstr "Eine laufende Anwendung verhindert momentan den automatischen Energiesparmodus. Wenn Sie diese Aktion jetzt jedoch durchführen, kann der Zustand der Anwendung beschädigt werden.\nSind Sie sicher, dass Sie das System jetzt in den Ruhezustand versetzen wollen?"

#: src/xfpm-power.c:291
msgid ""
"None of the screen lock tools ran successfully, the screen will not be locked.\n"
"Do you still want to continue to suspend the system?"
msgstr "Keines der Bildschirmsperrwerkzeuge läuft erfolgreich, der Bildschirm wird nicht gesperrt.\nWollen Sie immer noch damit fortfahren, das System in Bereitschaft zu versetzen?"

#: src/xfpm-power.c:452
msgid "Hibernate the system"
msgstr "Das System in den Ruhezustand versetzen"

#: src/xfpm-power.c:464
msgid "Suspend the system"
msgstr "Das System in den Bereitschaftsmodus versetzen"

#: src/xfpm-power.c:484
msgid "Shutdown the system"
msgstr "Das System herunterfahren"

#: src/xfpm-power.c:497 src/xfpm-power.c:528
msgid "System is running on low power. Save your work to avoid losing data"
msgstr "Ihr Akku ist nahezu leer. Sichern Sie Ihre Arbeit, damit keine Daten verloren gehen"

#: src/xfpm-power.c:585
msgid "_Cancel"
msgstr "_Abbrechen"

#: src/xfpm-power.c:706
msgid "System is running on low power"
msgstr "Das System läuft mit einem niedrigen Energiezustand"

#: src/xfpm-power.c:722
#, c-format
msgid ""
"Your %s charge level is low\n"
"Estimated time left %s"
msgstr "Die Ladung von %s ist niedrig\nGeschätzte verbleibende Zeit: %s"

#: src/xfpm-power.c:1409 src/xfpm-power.c:1453 src/xfpm-power.c:1489
#: src/xfpm-power.c:1523
msgid "Permission denied"
msgstr "Erlaubnis verweigert"

#: src/xfpm-power.c:1498
msgid "Hibernate not supported"
msgstr "Ruhezustand wird nicht unterstützt"

#: src/xfpm-power.c:1532
msgid "Suspend not supported"
msgstr "Der Bereitschaftsmodus wird nicht unterstützt"

#. generate a human-readable summary for the notification
#: src/xfpm-backlight.c:160
#, c-format
msgid "Brightness: %.0f percent"
msgstr "Helligkeit: %.0f Prozent"

#: src/xfpm-battery.c:109 src/xfpm-battery.c:160
#, c-format
msgid "Your %s is fully charged"
msgstr "Ihr %s ist vollständig geladen"

#: src/xfpm-battery.c:112 src/xfpm-battery.c:163
#, c-format
msgid "Your %s is charging"
msgstr "Ihr %s lädt"

#: src/xfpm-battery.c:122
#, c-format
msgid ""
"%s (%i%%)\n"
"%s until fully charged"
msgstr "%s (%i%%)\n%s bis zur vollständigen Ladung"

#: src/xfpm-battery.c:130 src/xfpm-battery.c:166
#, c-format
msgid "Your %s is discharging"
msgstr "Ihr %s wird entladen"

#: src/xfpm-battery.c:132
#, c-format
msgid "System is running on %s power"
msgstr "System läuft mit Strom vom %s"

#: src/xfpm-battery.c:142
#, c-format
msgid ""
"%s (%i%%)\n"
"Estimated time left is %s"
msgstr "%s (%i%%)\nGeschätzte verbleibende Zeit %s"

#: src/xfpm-battery.c:148 src/xfpm-battery.c:169
#, c-format
msgid "Your %s is empty"
msgstr "Ihr %s ist leer"

#. generate a human-readable summary for the notification
#: src/xfpm-kbd-backlight.c:117
#, c-format
msgid "Keyboard Brightness: %.0f percent"
msgstr "Tastaturhelligkeit: %.0f Prozent"

#: src/xfpm-main.c:56
#, c-format
msgid ""
"\n"
"Xfce Power Manager %s\n"
"\n"
"Part of the Xfce Goodies Project\n"
"http://goodies.xfce.org\n"
"\n"
"Licensed under the GNU GPL.\n"
"\n"
msgstr "\nXfce-Energieverwaltung %s\n\nTeil des Xfce-Goodies-Projektes\nhttp://goodies.xfce.org\n\nVeröffentlicht unter der GNU GPL.\n\n"

#: src/xfpm-main.c:114
msgid "With policykit support\n"
msgstr "Mit Unterstützung für »policykit«\n"

#: src/xfpm-main.c:116
msgid "Without policykit support\n"
msgstr "Ohne Unterstützung für »policykit«\n"

#: src/xfpm-main.c:131
msgid "Can suspend"
msgstr "Kann in Bereitschaftsmodus versetzen"

#: src/xfpm-main.c:133
msgid "Can hibernate"
msgstr "Kann in Ruhezustand versetzen"

#: src/xfpm-main.c:135
msgid "Authorized to suspend"
msgstr "Darf in Bereitschaftsmodus versetzen"

#: src/xfpm-main.c:137
msgid "Authorized to hibernate"
msgstr "Darf System in den Ruhezustand versetzen"

#: src/xfpm-main.c:139
msgid "Authorized to shutdown"
msgstr "Darf System herunterfahren"

#: src/xfpm-main.c:141
msgid "Has battery"
msgstr "Hat Akku"

#: src/xfpm-main.c:143
msgid "Has brightness panel"
msgstr "Hat Helligkeitsregler"

#: src/xfpm-main.c:145
msgid "Has power button"
msgstr "Hat Hauptschalter"

#: src/xfpm-main.c:147
msgid "Has hibernate button"
msgstr "Hat Taste für Ruhezustand"

#: src/xfpm-main.c:149
msgid "Has sleep button"
msgstr "Hat Energiesparmodustaste"

#: src/xfpm-main.c:151
msgid "Has battery button"
msgstr "Hat Akkutaste"

#: src/xfpm-main.c:153
msgid "Has LID"
msgstr "Hat Klappe"

#: src/xfpm-main.c:272
msgid "Daemonize"
msgstr "Als Dienst ausführen"

#: src/xfpm-main.c:274
msgid "Dump all information"
msgstr "Alle Informationen ausgeben"

#: src/xfpm-main.c:275
msgid "Restart the running instance of Xfce power manager"
msgstr "Die laufende Xfce-Energieverwaltung neustarten"

#: src/xfpm-main.c:276
msgid "Show the configuration dialog"
msgstr "Einstellungsdialog anzeigen"

#: src/xfpm-main.c:277
msgid "Quit any running xfce power manager"
msgstr "Xfce-Energieverwaltung beenden"

#: src/xfpm-main.c:278
msgid "Version information"
msgstr "Versionsinformation"

#: src/xfpm-main.c:300
#, c-format
msgid "Failed to parse arguments: %s\n"
msgstr "Fehler beim Analysieren der Argumente: %s\n"

#: src/xfpm-main.c:329
#, c-format
msgid "Type '%s --help' for usage."
msgstr "»%s --help« eingeben, um Benutzungsinformationen zu erhalten"

#: src/xfpm-main.c:350
msgid "Unable to get connection to the message bus session"
msgstr "Kann keine Verbindung zur Sitzung des Nachrichtenbusses erhalten"

#: src/xfpm-main.c:358 src/xfpm-main.c:407
msgid "Xfce power manager is not running"
msgstr "Die Xfce-Energieverwaltung läuft nicht"

#: src/xfpm-main.c:449
msgid "Another power manager is already running"
msgstr "Eine andere Energieverwaltung läuft bereits"

#: src/xfpm-main.c:453
msgid "Xfce power manager is already running"
msgstr "Xfce-Energieverwaltung läuft bereits"

#: src/xfpm-inhibit.c:394
msgid "Invalid arguments"
msgstr "Ungültige Argumente"

#: src/xfpm-inhibit.c:428
msgid "Invalid cookie"
msgstr "Ungültige Profildatei"

#: src/xfpm-manager.c:475
msgid ""
"None of the screen lock tools ran successfully, the screen will not be "
"locked."
msgstr "Keines der Programme zum Sperren des Bildschirms lief erfolgreich, der Bildschirm wird nicht gesperrt."

#: src/xfce4-power-manager.desktop.in:4
msgid "Power management for the Xfce desktop"
msgstr "Energieverwaltung für die Xfce-Arbeitsumgebung"

#. SECURITY:
#. - A normal active user on the local machine does not need permission
#. to change the backlight brightness.
#: src/org.xfce.power.policy.in.in:21
msgid "Modify the laptop display brightness"
msgstr "Ändern der Helligkeit des Laptop-Bildschirms"

#: src/org.xfce.power.policy.in.in:22
msgid "Authentication is required to modify the laptop display brightness"
msgstr "Um die Helligkeit des Laptop-Bildschirms zu ändern, ist eine Authentifizierung erforderlich."

#. SECURITY:
#. - A normal active user on the local machine does not need permission
#. to suspend or hibernate their system.
#: src/org.xfce.power.policy.in.in:36
msgid "Suspend or hibernate the system"
msgstr "Das System in Bereitschaft oder in den Ruhezustand versetzen"

#: src/org.xfce.power.policy.in.in:37
msgid ""
"Authentication is required to place the system in suspend or hibernate mode"
msgstr "Um das System in Bereitschaft oder in den Ruhezustand zu versetzen, ist eine Authentifizierung erforderlich."

#. Odds are this is a desktop without any batteries attached
#: panel-plugins/power-manager-plugin/power-manager-button.c:277
msgid "Display battery levels for attached devices"
msgstr "Anzeige des Ladezustands für angeschlossene Geräte"

#. Translators this is to display which app is inhibiting
#. * power in the plugin menu. Example:
#. * VLC is currently inhibiting power management
#: panel-plugins/power-manager-plugin/power-manager-button.c:1400
#, c-format
msgid "%s is currently inhibiting power management"
msgstr "%s blockiert derzeit die Energieverwaltung"

#: panel-plugins/power-manager-plugin/power-manager-button.c:1618
msgid "<b>Display brightness</b>"
msgstr "<b>Bildschirmhelligkeit</b>"

#: panel-plugins/power-manager-plugin/power-manager-button.c:1645
#: panel-plugins/power-manager-plugin/power-manager-button.c:1656
msgid "Presentation _mode"
msgstr "_Präsentationsmodus"

#. Power manager settings
#: panel-plugins/power-manager-plugin/power-manager-button.c:1669
msgid "_Settings..."
msgstr "_Einstellungen..."

#: panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:146
msgid "None"
msgstr "Nichts"

#: panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:146
msgid "Percentage"
msgstr "Prozent"

#: panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:146
msgid "Remaining time"
msgstr "Verbleibende Zeit"

#: panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:146
msgid "Percentage and remaining time"
msgstr "Prozent und verbleibende Zeit"

#. create the dialog
#: panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:154
msgid "Power Manager Plugin Settings"
msgstr "Einstellungen für die Energieverwaltungs-Erweiterung"

#. show-panel-label setting
#: panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:177
msgid "Show label:"
msgstr "Beschriftung anzeigen:"

#: panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:210
msgid "Show 'Presentation mode' indicator:"
msgstr "Zeige den Hinweis \"Präsentationsmodus\" an:"

#: panel-plugins/power-manager-plugin/xfce/power-manager-plugin.desktop.in.in:4
msgid "Power Manager Plugin"
msgstr "Energieverwaltungserweiterung"

#: panel-plugins/power-manager-plugin/xfce/power-manager-plugin.desktop.in.in:5
msgid ""
"Display the battery levels of your devices and control the brightness of "
"your display"
msgstr "Den Akkuladestand Ihrer Geräte anzeigen und die Helligkeit Ihres Bildschirms steuern"

#: data/appdata/xfce4-power-manager.appdata.xml.in:7
msgid "Xfce power manager"
msgstr "Xfce-Energieverwaltung"

#: data/appdata/xfce4-power-manager.appdata.xml.in:8
msgid "Manage and reduce power consumption of computer and devices"
msgstr "Verwalten und reduzieren des Stromverbrauchs von Rechner und Geräten"

#: data/appdata/xfce4-power-manager.appdata.xml.in:11
msgid ""
"Xfce power manager manages the power sources on the computer and the devices"
" that can be controlled to reduce their power consumption (such as LCD "
"brightness level, monitor sleep, CPU frequency scaling)."
msgstr "Die Xfce-Energieverwaltung verwaltet die Energiequellen des Rechners und die Geräte, die gesteuert werden können, um ihren Energieverbrauch zu reduzieren (wie zum Beispiel LCD-Helligkeit, Stromsparmodus des Bildschirms, CPU-Frequenzskalierung)."

#: data/appdata/xfce4-power-manager.appdata.xml.in:16
msgid ""
"In addition, Xfce power manager provides a set of freedesktop-compliant DBus"
" interfaces to inform other applications about current power level so that "
"they can adjust their power consumption, and it provides the inhibit "
"interface which allows applications to prevent automatic sleep actions via "
"the power manager; as an example, the operating system’s package manager "
"should make use of this interface while it is performing update operations."
msgstr "Zusätzlich stellt die Xfce-Energieverwaltung einen Satz an Freedesktop-konformen DBus-Schnittstellen zur Verfügung, um andere Anwendungen über den aktuellen Energiestand zu informieren, sodass diese ihren Energieverbrauch anpassen bzw. das automatische Versetzen in den Bereitschaftsmodus oder Ruhezustand verhindern können; beispielsweise sollte die Paketverwaltung des Betriebssystems diese Schnittstelle während der Aktualisierungsvorgänge benutzen."

#: data/appdata/xfce4-power-manager.appdata.xml.in:23
msgid ""
"Xfce power manager also provides a plugin for the Xfce and LXDE panels to "
"control LCD brightness levels and to monitor battery and device charge "
"levels."
msgstr "Die Xfce-Energieverwaltung stellt ebenfalls ein Erweiterung für die Xfce- und die LXDE-Leiste zur Verfügung, um die Bildschirmhelligkeit zu steuern und um die Akkuladung und die Geräteladung zu überwachen."

#: data/appdata/xfce4-power-manager.appdata.xml.in:34
msgid "Screenshot of the Xfce Power Manager User Interface"
msgstr "Screenshot der Benutzeroberfläche der Xfce Energieverwaltung"
